@extends('layouts.admin')

@section('breadcrumbs')
<nav aria-label="breadcrumb" >
  <ol class="breadcrumb">
    <li class="breadcrumb-item far active"><span>Dashboard</span></li>
  </ol>
</nav>
@stop

@section('header')
<header class="flex-center">
    <h1>{{ $title }}</h1>
</header>
@endsection

@section('content')
  <div class="row">
    <div class="col-sm-12">
      <div class="caboodle-card">
        <div class="caboodle-card-body">
          <div class="dash-quote p-5">
            <h2 class="text-center">{!!  $quote !!}</h2>
            <h4 class="text-right">- <i>{{ $quote_by }}</i></h3>
            </h4>
          </div>
        </div>
      </div>
    </div>
  </div>
@stop
