<?php

namespace App;

use App\Acme\Model\BaseModel;
use Illuminate\Database\Eloquent\Model;

class VideoBanner extends BaseModel
{
    
    protected $fillable = [
    	'frametag',

        'type',

        'option_id',

        'video_id',
    	];
    
    
    public function seo()
    {
        return $this->morphMany('App\Seo', 'seoable');
    }
    
    public function activities()
    {
        return $this->morphMany('App\Activity', 'loggable');
    }

    public function video_option()
	{
		return $this->hasMany('App\VideoOption', 'id', 'option_id');
	}
}
