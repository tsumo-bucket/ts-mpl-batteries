<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePageControlsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('page_controls', function (Blueprint $table) {
            $table->increments('id');
			$table->integer('reference_id');
            $table->string('reference_type');
			$table->string('name');
			$table->string('label');
			$table->enum('type', ['text', 'number', 'checkbox', 'textarea', 'asset', 'select', 'color']);
            $table->text('options_json');
            $table->tinyInteger('required')->default(0);
            $table->integer('order')->default(0);
			$table->text('value');
            $table->timestamps();
			$table->index(['reference_id', 'reference_type']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('page_controls');
    }
}
